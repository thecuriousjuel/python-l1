import numpy as np

def fun(array):
    l = array[0][0]
    s = array[0][0]

    lr = 0
    lc = 0

    sr = 0
    sc = 0


    for i in range(array.shape[0]):
        for j in range(array.shape[1]):
            if array[i][j] > l:
                l = array[i][j]
                lr = i
                lc = j
            
            if array[i][j] < s:
                s = array[i][j]
                sr = i
                sc = j

    return((l, ((lr+1), (lc+1))), (s, ((sr+1), (sc+1))))


ar = np.random.randint(1, 100, [3,3])
# print(ar)
print(fun(ar))
